package ru.zhelper.zhelper.models.users;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_TELEGRAM,
    ROLE_EMAIL
}
